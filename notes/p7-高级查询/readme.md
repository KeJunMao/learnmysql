# 高级查询
## 聚合函数
* `count` 对记录进行统计
    ```sql
    SELECT COUNT(*) from studInfo;
    SELECT COUNT(*) from studInfo WHERE studSex="女";
    ```
* `sum` 列求和，要求为数值类型
    ```sql
    SELECT SUM(score) AS "总成绩" FROM scoreInfo;
    SELECT SUM(score) AS "kj1331001总成绩" FROM scoreInfo WHERE studID="kj1331001"
    ```
* `avg` 列求平均值
    ```sql
    SELECT AVG(score) AS "平均成绩" FROM scoreInfo;
    SELECT AVG(score) AS "A01001平均成绩" FROM scoreInfo WHERE courseID="A01001";
    ```
* `max` 最大值
    ```sql
    SELECT MAX(score) AS "最高分" FROM scoreInfo;
    ```
* `min` 最小值
    ```sql
    SELECT MIN(score) AS "A01001最低分" FROM scoreInfo WHERE coureseID="A01001";
    ```
## 排序 ORDER BY ASC 升序|DESC 降序

```sql
SELECT * FROM studInfo;
SELECT * FROM studInfo ORDER BY studBirthday;
SELECT * FROM studInfo ORDER BY studBirthday DESC;
SELECT * FROM studInfo ORDER BY studSex,studBirthday DESC;
```

> 默认为升序

## 分组 

```sql
SELECT count(*),studSex FROM studInfo GROUP BY studSex;
SELECT count(*),classID FROM studInfo GROUP BY classID;
```
> SELECT 后面要么是`聚合函数`，要么是分组依据`列名`,否则统计出来的数据没有意义。

### 分组后条件 having

```sql
SELECT COUNT(*) AS "人数",classID FROM studInfo GROUP BY classID HAVING COUNT(*)<5;
```

> 有 having 必有 group by ，having 用于对分组后的数据进行条件查询

## 查询结果数 limit
```sql
SELECT * FROM studInfo;
SELECT * FROM studInfo LIMIT 5;
SELECT * FROM studInfo LIMIT 5,5;
```

## 其他函数

27,28,29,30,31,33,35,37,38,40,41,43