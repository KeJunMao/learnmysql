# 数据库操作

## 创建

```sql
CREATE  DATABASE [IF NOT EXITS] db_name [create_specification:character set charset] | collate collation_name
```

### 例子

1. 创建名为 `mydb1` 的数据库

   ```sql
   CREATE DATABASE mydb1;
   ```

2. 创建使用 `gbk` 字符集名为 `mydb2` 的数据库

   ```sql
   CREATE DATABASE mydb2 character set gbk;
   ```

3. 创建使用 `gbk` 字符集,并带校对规则的名为 mydb3 的数据库

   ```sql
   CREATE DATABASE mydb3 character set gbk collate gbk_binn;
   ```

## 修改

```sql
ALTER DATABASE db_name [[default]character  set cgarset | [default]collate collation_name];
```

### 例子

1. 修改mybd3数据库的字符集为 `utf8`，校对为 `utf8_bin`

   ```sql
   ALTER DATABASE mydb3 default character set utf8 collate utf8_bin;
   ```

## 删除

```sql
DROP  DATABASE  db_name;
```

### 例子

1. 删除数据库 `mydb3`

   ```sql
   DROP DATABASE mydb3;
   ```

## 查询

```sql
SHOW DATABASES; -- 查看所有数据库

SHOUW CREATE  DATABASE  db_name; -- 查看指定数据库
```

# 表操作

## 创建表

```sql
CREATE TABLE table_name(
    field1 datatype,
    field2 datatype,
    ...
    fieldn datatype
);
```

### 例子

1. 创建一个名为 `employee` 的员工表

    |字段|类型|`datatype`|
    |---|---|---|
    |id |整型|INT|
    |name|字符型|VARCHAR(20)|
    |gender|字符型/二进制|CHAR(1)/BIT|
    |birthday|日期|DATE|
    |salary|浮点型|DOUBLE|
    |job|字符型|VARCHAR(50)|
    |resume|文本型|TEXT|

```sql
-- 创建 yrm 数据库
CREATE DATABASE yrm;

-- 打开数据库

USE yrm;

-- 创建表

CREATE TABLE employee(
    id int,
    name varchar(30),
    gender char(5),
    birthday date,
    salary double,
    job varchar(50),
    resume text
);
```

## 查看表

* 查看当前数据库所有表：

    ```sql
    SHOW tables;
    ```
* 查看表结构：
    ```sql
    DESC table_name;
    ```
* 查看建表的语句：
    ```sql
    SHOW CREATE TABLE table_name;
    ```