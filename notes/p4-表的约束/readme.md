# 表的约束

## 主键约束（`PRIMARY KEY`）

* 单一字段作为主键

    ```sql
    CREATE TABLE table_name{
        ...
        field dataytpe PRIMARY KEY,
    }
    ```

* 多个字段联合作为主键

    ```sql
    CREATE TABLE table_name(
        ...,
        PRIMARY KEY(
            field1,
            field2
        )
    )
    ```

## 非空约束(`NOT NULL`)

```sql
CREATE TABLE table_name(
    ...,
    field datatype NOT NULL,
)
```

## 唯一性约束(`UNIQUE`)

```sql
CREATE TABLE table_name(
    ...,
    field datatype UNIQUE,
)
```
## 默认值约束(`DEFAULT`)

```sql
CREATE TABLE table_name(
    ...,
    field datatype DEFAULT,
)
```

## 外键约束(`FOREIGN KEY`)

```sql
CREATE TABLE table_name(
    ...,
    field datatype,
    FOREIGN KEY(field) REFERENCES reference_table(field)
)
```

```sql
CREATE TABLE table_name(
    ...,
    field datatype,
    -- CONSTRAINT 约束通用
    CONSTRAINT foreign_name FOREIGN KEY(field) REFERENCES reference_table(field)
)
```
## 自动增加

```sql
CREATE TABLE table_name(
    ...,
    field datatype AUTO_INCREMENT,
)
```

## 例子

1. 创建带有主键,非空约束，唯一性约束，默认值约束的的数据表 `userinfo`

```sql
CREATE TABLE userinfo(
    id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(20) NOT NULL UNIQUE,
    job VARCHAR(20) DEFAULT 'six'
);
```

2. 创建一个带有外键表的`t1` ,使 `stu_id` 关联到 `userinfo` 的主键

```sql
CREATE TABLE t1(
    id INT AUTO_INCREMENT,
    stu_id INT,
    name VARCHAR(20),
    FOREIGN KEY(stu_id) REFERENCES userinfo(id)
);
```

# 索引

```sql
CREATE TABLE table_name(
    field datatype [constraints],
    field datatype [constraints],
    ...
    field datatype [UNIQUE|FULLTEXT|SPATIAL] INDEX|KEY [alias](field1[(length)] [ASC|DESC]
)
```

## 普通索引

```sql
CREATE TABLE table_name(
    id INT PRIMARY KEY,
    name VARCHAR(20),
    INDEX(id)
)
```

## 唯一性索引

```sql
CREATE TABLE table_name(
    id INT UNIQUE,
    name VARCHAR(20),
    UNIQUE INDEX uni_id(id)
)
```

## 全文索引(MyISAM)

## 单列索引

```sql
CREATE TABLE table_name(
    id INT,
    name VARCHAR(20),
    INDEX index_name(name(20))
)
```

## 多列索引

```sql
CREATE TABLE table_name(
    id INT,
    name VARCHAR(20),
    sex char(2),
    INDEX index_name(name,sex(20))
);
```

## 空间索引(MyISAM)