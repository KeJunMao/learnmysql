# 数据操作
> 查看所有数据
```sql
SELECT * FROM table_name
```
## 添加数据
INSERT INTO
### 给表中所有字段添加数据
1. 指定字段
    ```sql
    INSERT INTO table_name(field1,field2...) VALUES (value1,value2)
    ```
2. 不指定字段
    ```sql
    INSERT INTO table_name VALUES (value1,value2)
    ```
### 给表中部分字段添加数据
```sql
INSERT INTO table_name(field1,field2...) VALUES (value1,value2)
```
### 使用set给表添加数据
```sql
INSERT INTO table_name SET field1=value1,field2=value2
```

## 修改
UPDATE
### 修改所有记录
```sql
UPDATE table_name set field1=value1,field2=value2...

UPDATE t1 set age=20
```
### 修改部分记录
```sql
UPDATE table_name SET field1=value1,field2=value2... where condition

UPDATE t1 SET name="zhang" WHERE sex="男" or age=20
```
> WHERE 条件，指定修改的范围

## 删除
DELETE
### 删除所有
```sql
DELETE FROM table_name

TRUNCATE TABLE table_name
```
### 删除部分
```sql
DELETE FROM table_name where condition

DELETE FROM table_name where name="zhang"
``sql