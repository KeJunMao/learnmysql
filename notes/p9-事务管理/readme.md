```sql
create table account(
    id int primary key AUTO_INCREMENT,
    name varchar(40),
    money float
);

insert into account values(1,'a',1000)
insert into account values(2,'b',1000)

start transaction;
update account set money=money+100 where name='a';
update account set money=money-100 where name='b';
commit;
rollback;

-- 设置隔离级别
--- repeatable read 可重复读
--- read 读未提交
--- read committed 读提交
--- serializable 可串行读
set session transaction isolation level read committed 
```

page 141 