# 多表操作
## 外键

### 添加外键约束

```sql
ALTER TABLE table_name1 ADD CONSTRAINT forging_key_name FOREIGN KEY(field1) REFERENCES table_name2 (field2);

ALTER TABLE 表名 ADD CONSTRAINT 外键名 FOREIGN KEY(外键字段名) REFERENCES 外表表名 (主键字段名);
```

## 操作关联表

```
emmmmm
```
## 连接查询

### 交叉连接
CROSS JOIN
```sql
SELECT * FROM table_name1 CROSS JOIN table_name2
```

### 内连接
INNER JOIN ... ON
```sql
select * from student inner join grade on student.gid=grade.id;

select * from student inner join grade on student.gid=grade.id where sname like '%m%';
```

### 外连接

* 左连接 LEFT JOIN
* 右连接 RIGHT JOIN

### 复合条件连接查询

5.1.2 5.2.2 

example 5-1 5-4 5-5 5-6