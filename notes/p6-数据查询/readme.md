# 数据查询
```sql
SELECT [DISTINCT] *|{field1,field2...}
    FROM table_name
    [WHERE condition]
    [GROUP BY field [HAVING condition]]
    [ORDER BY field [ASC|DESC]]
    [LIMIT [OFFSET] offset_num]
```

## 简单查询

### 查询所有
```sql
SELECT * FROM table_name;
```
### 查询部分
```sql
SELECT field1,field2... FROM table_name;
```

## 条件查询
### 关系运算符
```sql
SELECT * FROM table_name WHERE num<70;
SELECT * FROM user_info WHERE sex="男";
```
### IN 集合运算符
```sql
SELECT * FROM table_name WHERE field1 IN ('kj1331','wl1332')
```
### between...and (表示范围的)
```sql
SELECT * FROM table_name WHERE field1 BETWEEN 70 and 90;
-- NOT 表示取反
SELECT * FROM table_name WHERE field1 NOT BETWEEN 70 and 90;
```
### 空值
```sql
SELECT * FROM table_name WHERE field1 IS NULL
```
### DISTINCT (去除重复记录)

```sql
SELECT DISTINCT field1 FROM table_name 
```

### LIKE 模糊查询
```sql
SELECT * FROM table_name where field1 LIKE '张%'
SELECT * FROM table_name where field1 LIKE '张_'
```
* `%` 表示零个或任意多个字符
*  `_` 表示一个字符，`__` 表示两个字符
* `\` 用于转义

> `in` 和 `between...and` 以及 `like` 都可以前置 `not` 用于取反

### 多条件查询 and or
```sql
SELECT * FROM table_name WHERE field1='value1' or field2='value2'

SELECT * FROM table_name WHERE classID="wl1331" and studSex="男";
```

