# 多表查询
## 子查询

### 带关键字 in
查询选修了课程的学生学号,姓名,班级号

```sql
SELECT studID,studName,classID from studInfo where studID IN (SELECT studID from scoreInfo)

SELECT studInfo.studID,studName,classID FROM studInfo,scoreInfo WHERE studInfo.studID=scoreInfo.studID
```
> distinct 去重复

### 带关键字 exists
查询选修了课程的学生学号、姓名、班级号
```sql
SELECT studID,studName,classID FROM studInfo where EXISTS(SELECT * FROM scoreInfo where studID=studInfo.studID)
```

### 带比较运算符

通常与 any 和 all 混用

### 带 any 和 all 运算符
查询kj1331班中，出生日期大于wl1331班所有人的同学
```sql
SELECT * FROM studInfo where classID='kj1331' AND studBirthday > ALL(SELECT studBirthday FROM studInfo where classID='wl1331')

SELECT * FROM studInfo where classID='kj1331' AND studBirthday > ANY(SELECT studBirthday FROM studInfo where classID='wl1331')
```

5-8 5-10 5-11 5-12 5-13 