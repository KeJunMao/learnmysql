## 修改表

### 增加字段

```sql
ALTER TABLE table_name add(
    field datetype
);
```

### 修改字段的数据类型
```sql
ALTER TABLE table_name MODIFY field datatype;
```

### 修改字段名

```sql
ALTER TABLE table_name CHANGE old_field new_field datatype;
```

### 删除字段

```sql
ALTER TABLE table_name DROP field;
```

### 修改表名

```sql
ALTER TABLE old_table_name to new_table_name;

-- or

RENAME TABLE old_table_name to new_table_name;
```

### 修改字符集

```sql
ALTER TABLE table_name CHARACTER SET charset;
```

### 修改字段的顺序

```sql
ALTER TABLE table_name MODIFY field datatype FIRST|AFTER field2;
```

### 例子

1. 在员工表 `employee` 中增加一个 `image` 字段， 类型 `blod`

    ```sql
    ALTER TABLE employee add(
        image blob
    );
    ```
2. 将 `job` 的长度修改为 `60`
    ```sql
    ALTER TABLE employee MODIFY job varchar(60);
    ```
3. 修改 `name` 为 `username`
    ```sql
    ALTER TABLE employee CHANGE name username varchar(30);
    ```
4. 删除 `gender` 字段
    ```sql
    ALTER TABLE employee DROP gender;
5. 将表名改为 `user` 表
    ```sql
    RENAME TABLE employee to user;
    ```
6. 将 `username` 改为表的第一个字段

    ```sql
    ALTER TABLE user MODIFY username varchar(30) FIRST;
    ```

## 删除表

```sql
DROP TABLE table_name
```
